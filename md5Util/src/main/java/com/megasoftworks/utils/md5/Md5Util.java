/**
 * Clase Md5Util.java mar 26, 2019
 */
package com.megasoftworks.utils.md5;

import com.megasoftworks.utils.md5.exceptions.CreacionMd5Exception;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author pabloaleman
 */
public final class Md5Util {

    private static final Logger LOGGER = LoggerFactory.getLogger(Md5Util.class);

    private Md5Util() {

    }

    /**
     * Funcion que calcula el md5 de un inputstream.
     *
     * @param file el inputstream del archivo a calcular el md5.
     * @return el md5 del archivo dado.
     * @throws CreacionMd5Exception en caso de errores.
     */
    public static String fileMd5Sum(InputStream file) throws CreacionMd5Exception {
        try {
            String md5 = DigestUtils.md5Hex(file);
            file.close();
            return md5;
        } catch (IOException e) {
            LOGGER.error("Error creando el md5", e);
            throw new CreacionMd5Exception(e);
        }
    }

    /**
     * Funcion que calcula el md5 de un archivo.
     *
     * @param path el path del archivo a comprimir.
     * @return el md5 del archivo dado.
     * @throws CreacionMd5Exception en caso de errores.
     */
    public static String fileMd5Sum(String path) throws CreacionMd5Exception {
        try {
            return fileMd5Sum(new FileInputStream(path));
        } catch (FileNotFoundException e) {
            LOGGER.error(String.format("Archivo %s no encontrado", path));
            throw new CreacionMd5Exception(e);
        }
    }

    /**
     * Funcion que calcula el md5 de una cadena dada.
     *
     * @param md5 la cadena a calcular el md5.
     * @return el md5 del archivo dado.
     */
    public static String toMD5(String md5) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("Error creando el md5", e);
        }
        return null;
    }

}
