/**
 * Clase CreacionMd5Exception.java mar 26, 2019
 */
package com.megasoftworks.utils.md5.exceptions;

/**
 * @author pabloaleman
 */
public class CreacionMd5Exception extends Exception {
    private static final long serialVersionUID = 2514366316024614764L;
    public CreacionMd5Exception(Throwable t) {
        super(t);
    }
}
