/**
 * Clase SecuencialesRandomicosUtil.java mar 22, 2019
 */
package com.megasoftworks.utils.secuenciales.randomicos;

import java.security.SecureRandom;

/**
 * @author pabloaleman
 */
public final class SecuencialesRandomicosUtil {
    private SecuencialesRandomicosUtil() {
    }

    /**
     * @param lengthChar el tamanio de la cadena a retornar.
     * @param numeros    si deben haber numeros en la secuencia.
     * @param mayusculas si deben haber mayusculas en la secuencia.
     * @param minusculas si deben haber minusculas en la secuencia.
     * @return la cadena randomica
     */
    public static String generateSecuenceChar(int lengthChar, boolean numeros, boolean mayusculas, boolean minusculas) {
        StringBuilder cadenaCodeSB = new StringBuilder();
        if (numeros) {
            cadenaCodeSB.append("0123456789");
        }
        if (mayusculas) {
            cadenaCodeSB.append("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        }
        if (minusculas) {
            cadenaCodeSB.append("abcdefghijklmnopqrstuvwxyz");
        }
        SecureRandom rnd = new SecureRandom();

        StringBuilder cadenaTmp = new StringBuilder(lengthChar);
        String cadenaCode = cadenaCodeSB.toString();

        for (int i = 0; i < lengthChar; i++) {
            cadenaTmp.append(cadenaCode.charAt(rnd.nextInt(cadenaCode.length())));
        }
        return cadenaTmp.toString();

    }
}
