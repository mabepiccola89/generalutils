/**
 * Clase SecuencialesRandomicosUtilTest.java mar 27, 2019
 */
package com.megasoftworks.utils.secuenciales.randomicos.test;

import org.junit.Assert;
import org.junit.Test;

import com.megasoftworks.utils.secuenciales.randomicos.SecuencialesRandomicosUtil;

/**
 * @author pabloaleman
 */
public class SecuencialesRandomicosUtilTest {

    private static final String soloMayusculas =  "[A-Z]{5}";
    private static final String soloMinusculas =  "[a-z]{5}";
    private static final String soloNumeros =  "[0-9]{5}";
    private static final String todos =  "[A-Za-z0-9]{5}";
    private static final String mensajeError = "Error en la prueba";
    private static final int tamanio = 5;

    @Test
    public void generateSecuenceChar() {
        String retorno = SecuencialesRandomicosUtil.generateSecuenceChar(tamanio, false, true, false);
        Assert.assertTrue(mensajeError, retorno.matches(soloMayusculas));

        retorno = SecuencialesRandomicosUtil.generateSecuenceChar(tamanio, false, false, true);
        Assert.assertTrue(mensajeError, retorno.matches(soloMinusculas));

        retorno = SecuencialesRandomicosUtil.generateSecuenceChar(tamanio, true, false, false);
        Assert.assertTrue(mensajeError, retorno.matches(soloNumeros));

        retorno = SecuencialesRandomicosUtil.generateSecuenceChar(tamanio, true, true, true);
        Assert.assertTrue(mensajeError, retorno.matches(todos));
    }

}
