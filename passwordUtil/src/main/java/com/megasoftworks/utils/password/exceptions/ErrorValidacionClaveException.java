/**
 * Clase ErrorValidacionClaveException.java mar 21, 2019
 */
package com.megasoftworks.utils.password.exceptions;

/**
 * @author pabloaleman
 */
public class ErrorValidacionClaveException extends Exception {
    private static final long serialVersionUID = -8773135420842104077L;

    public ErrorValidacionClaveException(Throwable e) {
        super(e);
    }
}
