/**
 * Clase GenerarClaveException.java mar 21, 2019
 */
package com.megasoftworks.utils.password.exceptions;

/**
 * @author pabloaleman
 */
public class GenerarClaveException extends Exception {
    private static final long serialVersionUID = 7725446381914457876L;

    public GenerarClaveException(Throwable e) {
        super(e);
    }
}
