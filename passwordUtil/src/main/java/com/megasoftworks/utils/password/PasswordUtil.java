/**
 * Clase PasswordUtil.java mar 21, 2019
 */
package com.megasoftworks.utils.password;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.megasoftworks.utils.password.exceptions.ClaveIncorrectaException;
import com.megasoftworks.utils.password.exceptions.ErrorValidacionClaveException;
import com.megasoftworks.utils.password.exceptions.GenerarClaveException;

/**
 * @author pabloaleman
 */
public final class PasswordUtil {
    private static final Logger log = LoggerFactory.getLogger(PasswordUtil.class.getName());

    private PasswordUtil() {
    }

    /**
     * Funcion que genera el password encriptado a partir de una cadena.
     *
     * @param password el password generado.
     * @return la clave generada.
     * @throws GenerarClaveException en caso de errores.
     */
    public static String generatePassword(String password) throws GenerarClaveException {
        try {
            String passwordGen;
            int iterations = 1000;
            char[] chars = password.toCharArray();
            byte[] salt = getSalt();
            PBEKeySpec spec = new PBEKeySpec(chars, salt, iterations, 64 * 8);
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            byte[] hash = skf.generateSecret(spec).getEncoded();
            passwordGen = iterations + ":" + toHex(salt) + ":" + toHex(hash);
            return passwordGen;
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            log.error("Error al generar el password", e);
            throw new GenerarClaveException(e);
        }
    }

    /**
     * Funcion que valida si un password es correcto o no.
     *
     * @param claveCruda    la clave cruda a evaluar.
     * @param claveComparar la clave a comparar generada previamente.
     * @throws ClaveIncorrectaException      en caso de clave incorrecta.
     * @throws ErrorValidacionClaveException en caso de error en la validacion.
     */
    public static void validatePassword(String claveCruda, String claveComparar) throws ClaveIncorrectaException,
            ErrorValidacionClaveException {
        try {
            String[] parts = claveComparar.split(":");
            int iteractions = Integer.parseInt(parts[0]);
            byte[] salt = fromHex(parts[1]);
            byte[] hash = fromHex(parts[2]);

            PBEKeySpec spec = new PBEKeySpec(claveCruda.toCharArray(), salt, iteractions, hash.length * 8);
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            byte[] validateHash = skf.generateSecret(spec).getEncoded();

            int diff = hash.length ^ validateHash.length;
            for (int i = 0; i < hash.length && i < validateHash.length; i++) {
                diff |= hash[i] ^ validateHash[i];
            }

            if (diff != 0) {
                throw new ClaveIncorrectaException();
            }
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            log.error("Error al generar el password", e);
            throw new ErrorValidacionClaveException(e);
        }
    }

    private static byte[] getSalt() throws NoSuchAlgorithmException {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[16];
        sr.nextBytes(salt);
        return salt;
    }

    private static String toHex(byte[] array) {
        BigInteger bi = new BigInteger(1, array);
        String hex = bi.toString(16);
        int paddingLength = (array.length * 2) - hex.length();
        if (paddingLength > 0) {
            return String.format("%0" + paddingLength + "d", 0) + hex;
        } else {
            return hex;
        }
    }

    private static byte[] fromHex(String hex) {
        byte[] bytes = new byte[hex.length() / 2];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
        }

        return bytes;
    }
}
