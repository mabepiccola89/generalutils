/**
 * Clase PasswordUtilTest.java mar 19, 2019
 */
package com.megasoftworks.utils.password.test;

import com.megasoftworks.utils.password.PasswordUtil;
import com.megasoftworks.utils.password.exceptions.ClaveIncorrectaException;
import com.megasoftworks.utils.password.exceptions.ErrorValidacionClaveException;
import com.megasoftworks.utils.password.exceptions.GenerarClaveException;
import org.junit.Assert;
import org.junit.Test;


/**
 * @author pabloaleman
 */
public class PasswordUtilTest {

    @Test
    public void pruebaGeneral() {
        String error = "Error en la prueba";
        try {
            String claveCruda = "12345";
            String claveGenerada;
            claveGenerada = PasswordUtil.generatePassword(claveCruda);
            PasswordUtil.validatePassword(claveCruda, claveGenerada);
        } catch (GenerarClaveException | ClaveIncorrectaException | ErrorValidacionClaveException e) {
            Assert.fail(error);
        }
    }
}
