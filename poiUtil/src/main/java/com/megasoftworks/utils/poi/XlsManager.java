/**
 * Clase XlsManager.java abr 01, 2019
 */
package com.megasoftworks.utils.poi;

import com.megasoftworks.utils.poi.exceptions.ErrorLecturaXlsException;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.util.Iterator;

/**
 * @author pabloaleman
 */
public final class XlsManager extends XlsManagerPadre {
    static Logger logger = LoggerFactory.getLogger(XlsManager.class);

    private XlsManager() {
    }

    /**
     * Funcion que recibe un {@link FileInputStream} de un archivo xls y retorna el contenido representado en un string.
     *
     * @param file      el archivo a leer.
     * @param separator el separador de columnas en el string.
     * @return El string que representa el contenido del archivo.
     * @throws ErrorLecturaXlsException en caso de error.
     */
    public static String readXlsFile(FileInputStream file, String separator) throws ErrorLecturaXlsException {
        try {
            POIFSFileSystem fs = new POIFSFileSystem(file);
            HSSFWorkbook wb = new HSSFWorkbook(fs);
            HSSFSheet sheet = wb.getSheetAt(0);
            Iterator<Row> rowIterator = sheet.iterator();
            return barrerIterador(rowIterator, separator);
        } catch (Exception e) {
            logger.error("Error en la lectura del archivo", e);
            throw new ErrorLecturaXlsException(e);
        }
    }
}
