/**
 * Clase XlsxManager.java abr 01, 2019
 */
package com.megasoftworks.utils.poi;

import com.megasoftworks.utils.poi.exceptions.ErrorLecturaXlsException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.util.Iterator;

/**
 * @author pabloaleman
 */
public final class XlsxManager extends XlsManagerPadre {
    static Logger logger = LoggerFactory.getLogger(XlsxManager.class);

    private XlsxManager() {

    }

    /**
     * Funcion que recibe un {@link FileInputStream} de un archivo xlsx y retorna el contenido representado en un string.
     *
     * @param file      el archivo a leer.
     * @param separator el separador de columnas en el string.
     * @return El string que representa el contenido del archivo.
     * @throws ErrorLecturaXlsException en caso de error.
     */
    public static String readXlsxFile(FileInputStream file, String separator) throws ErrorLecturaXlsException {
        try {
            XSSFWorkbook myWorkBook = new XSSFWorkbook(file);
            XSSFSheet mySheet = myWorkBook.getSheetAt(0);
            Iterator<Row> rowIterator = mySheet.iterator();
            return barrerIterador(rowIterator, separator);
        } catch (Exception e) {
            logger.error("Error en la lectura del archivo", e);
            throw new ErrorLecturaXlsException(e);
        }
    }

}
