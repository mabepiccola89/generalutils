/**
 * Clase LectoresTest.java abr 01, 2019
 */
package com.megasoftworks.utils.poi.test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.junit.Assert;
import org.junit.Test;

import com.megasoftworks.utils.poi.XlsManager;
import com.megasoftworks.utils.poi.XlsxManager;
import com.megasoftworks.utils.poi.exceptions.ErrorLecturaXlsException;

/**
 * @author pabloaleman
 */
public class LectoresTest {
    private static final String pathArchivosSrc = "src/test/resources/";
    private static final String separador = "$$";
    private static final String textoEsperado = "linea1 columnaa$$\n" +
            "linea2 columnaa$$linea2 columna2$$\n" +
            "linea3 columnaa$$linea3 columnab$$linea3 columna3$$\n";

    @Test
    public void lectorXlsTest() {
        try {
            String lectura = XlsManager.readXlsFile(new FileInputStream(pathArchivosSrc + "hoja.xls"), separador);
            Assert.assertEquals(textoEsperado, lectura);
        } catch (FileNotFoundException | ErrorLecturaXlsException e) {
            Assert.fail();
        }
    }

    @Test
    public void lectorXlsTestError() {
        try {
            XlsManager.readXlsFile(new FileInputStream(pathArchivosSrc + "hoja.xlsx"), separador);
        } catch (FileNotFoundException | ErrorLecturaXlsException e) {
            Assert.assertNotNull(e);
        }
    }

    @Test
    public void lectorXlsxTest() {
        try {
            String lectura = XlsxManager.readXlsxFile(new FileInputStream(pathArchivosSrc + "hoja.xlsx"), separador);
            Assert.assertEquals(textoEsperado, lectura);
        } catch (FileNotFoundException | ErrorLecturaXlsException e) {
            Assert.fail();
        }
    }

    @Test
    public void lectorXlsxTestError() {
        try {
            XlsxManager.readXlsxFile(new FileInputStream(pathArchivosSrc + "hoja.xls"), separador);
        } catch (FileNotFoundException | ErrorLecturaXlsException e) {
            Assert.assertNotNull(e);
        }
    }

    @Test
    public void lectorXlsxTestErrorNull() {
        try {
            XlsxManager.readXlsxFile(null, separador);
        } catch (ErrorLecturaXlsException e) {
            Assert.assertNotNull(e);
        }
    }
}
