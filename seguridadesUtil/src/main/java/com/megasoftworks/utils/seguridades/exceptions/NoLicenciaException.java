/**
 * Clase NoLicenciaException.java abr 02, 2019
 */
package com.megasoftworks.utils.seguridades.exceptions;

/**
 * @author pabloaleman
 */
public class NoLicenciaException extends Exception {

    private static final long serialVersionUID = 3019129709803734988L;
}
