/**
 * Clase SeguridadesUtil.java abr 02, 2019
 */
package com.megasoftworks.utils.seguridades;

import com.megasoftworks.utils.fechas.FechasUtil;
import com.megasoftworks.utils.fechas.exceptions.ConversionFechaException;
import com.megasoftworks.utils.password.PasswordUtil;
import com.megasoftworks.utils.password.exceptions.ClaveIncorrectaException;
import com.megasoftworks.utils.password.exceptions.ErrorValidacionClaveException;
import com.megasoftworks.utils.seguridades.beans.DireccionMac;
import com.megasoftworks.utils.seguridades.exceptions.ErrorComparacionLicenciaException;
import com.megasoftworks.utils.seguridades.exceptions.LicenciaExpiradaException;
import com.megasoftworks.utils.seguridades.exceptions.NoLicenciaException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;

/**
 * @author pabloaleman
 */
public final class SeguridadesUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(SeguridadesUtil.class);

    private SeguridadesUtil() {

    }

    public static void comprobarLicencia(String licencia, String fechaExpiracion) throws NoLicenciaException, LicenciaExpiradaException, ErrorComparacionLicenciaException {
        compararFechaExpiracionActual(fechaExpiracion);
        comprobarLicencias(licencia, fechaExpiracion);
    }

    public static void comprobarLicencia(String licencia) throws NoLicenciaException, ErrorComparacionLicenciaException {
        comprobarLicencias(licencia, null);
    }

    private static void comprobarLicencias(String licencia, String fechaExpiracion) throws NoLicenciaException,
            ErrorComparacionLicenciaException {

        List<DireccionMac> direccionesMac = MacAddressUtil.obtenerDireccionesMac();
        boolean licenciaValida = false;
        for (DireccionMac direccionMac : direccionesMac) {
            String cadena = fechaExpiracion == null ? direccionMac.getDireccionMacComputador() : direccionMac.getDireccionMacComputador().concat(fechaExpiracion);
            try {
                PasswordUtil.validatePassword(cadena, licencia);
                licenciaValida = true;
                break;
            } catch (ErrorValidacionClaveException e) {
                LOGGER.error("Error al validar la licencia", e);
                throw new ErrorComparacionLicenciaException();
            } catch (ClaveIncorrectaException e) {
                LOGGER.debug("La direccion mac no es", e);
            }
        }
        if (!licenciaValida) {
            throw new NoLicenciaException();
        }
    }

    private static void compararFechaExpiracionActual(String fecha) throws LicenciaExpiradaException,
            ErrorComparacionLicenciaException {
        try {
            Date fechaDate = FechasUtil.stringToDate(fecha);
            if (fechaDate.before(new Date())) {
                LOGGER.info("La licencia expiro el {}, por favor contacte a la empresa", fecha);
                throw new LicenciaExpiradaException(fecha);
            }
        } catch (ConversionFechaException e) {
            LOGGER.error("Error al obtener fechas de licencias");
            throw new ErrorComparacionLicenciaException();
        }
    }
}
