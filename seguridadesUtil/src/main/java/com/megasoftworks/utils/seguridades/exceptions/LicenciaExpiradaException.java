/**
 * Clase LicenciaExpiradaException.java abr 03, 2019
 */
package com.megasoftworks.utils.seguridades.exceptions;

/**
 * @author pabloaleman
 */
public class LicenciaExpiradaException extends Exception {

    private static final long serialVersionUID = 6895622970795949947L;

    public LicenciaExpiradaException(String fecha) {
        super(String.format("Su licencia expiró el %s por favor póngase en contacto con la empresa para renovar su licencia.", fecha));
    }
}
