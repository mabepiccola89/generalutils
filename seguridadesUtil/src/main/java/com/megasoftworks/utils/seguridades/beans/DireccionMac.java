/**
 * Clase DireccionMac.java abr 03, 2019
 */
package com.megasoftworks.utils.seguridades.beans;

/**
 * @author pabloaleman
 */
public class DireccionMac {
    private String nombreInterface;
    private String direccionMacComputador;

    public String getNombreInterface() {
        return nombreInterface;
    }

    public void setNombreInterface(String nombreInterface) {
        this.nombreInterface = nombreInterface;
    }

    public String getDireccionMacComputador() {
        return direccionMacComputador;
    }

    public void setDireccionMacComputador(String direccionMacComputador) {
        this.direccionMacComputador = direccionMacComputador;
    }

    @Override
    public String toString() {
        return String.format("%s: %s", nombreInterface, direccionMacComputador);
    }
}
