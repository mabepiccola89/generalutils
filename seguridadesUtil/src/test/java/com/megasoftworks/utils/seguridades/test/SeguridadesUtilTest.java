/**
 * Clase SeguridadesUtilTest.java abr 03, 2019
 */
package com.megasoftworks.utils.seguridades.test;

import com.megasoftworks.utils.fechas.FechasUtil;
import com.megasoftworks.utils.password.PasswordUtil;
import com.megasoftworks.utils.password.exceptions.GenerarClaveException;
import com.megasoftworks.utils.seguridades.MacAddressUtil;
import com.megasoftworks.utils.seguridades.SeguridadesUtil;
import com.megasoftworks.utils.seguridades.beans.DireccionMac;
import com.megasoftworks.utils.seguridades.exceptions.ErrorComparacionLicenciaException;
import com.megasoftworks.utils.seguridades.exceptions.LicenciaExpiradaException;
import com.megasoftworks.utils.seguridades.exceptions.NoLicenciaException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author pabloaleman
 */
public class SeguridadesUtilTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(FechasUtil.class);

    private static final String direccionMacInventada = "7824AF3B5FA3";
    private static final String fechaExpiracionInventada = "31/12/2119";
    private static final String fechaExpiracionExpiradaInventada = "31/12/2018";
    private static String claveGeneradaConFecha;
    private static String claveGeneradaSinFecha;

    private static String claveGeneradaConFechaConMacInventada;
    private static String claveGeneradaSinFechaConMacInventada;
    private static String claveGeneradaConFechaExpirada;

    @Test
    public void generarLicencia() {
        String direccionMacCliente = "7824AF3B5FB4";
        String fechaExpiracionCliente = "31/12/2019";
        boolean aplicaFechaExpiracion = true;
        try {
            String cadenaEncriptar = direccionMacCliente;
            if (aplicaFechaExpiracion) {
                cadenaEncriptar = cadenaEncriptar.concat(fechaExpiracionCliente);
            }
            LOGGER.info(String.format("Licencia generada para cliente: %s", PasswordUtil.generatePassword(cadenaEncriptar)));
        } catch (GenerarClaveException e) {
            Assert.assertNotNull(e);
        }
    }

    @Before
    public void generarLicencias() {
        try {
            List<DireccionMac> direccionesMac = MacAddressUtil.obtenerDireccionesMac();
            String direccion = direccionesMac.get(0).getDireccionMacComputador();
            LOGGER.info(String.format("Direccion MAC a utilizar %s", direccion));
            claveGeneradaConFecha = PasswordUtil.generatePassword(direccion.concat(fechaExpiracionInventada));
            LOGGER.info(String.format("Licencia prueba con fecha: %s", claveGeneradaConFecha));
            claveGeneradaSinFecha = PasswordUtil.generatePassword(direccion);
            LOGGER.info(String.format("Licencia prueba sin fecha: %s", claveGeneradaSinFecha));

            claveGeneradaConFechaConMacInventada = PasswordUtil.generatePassword(direccionMacInventada.concat(fechaExpiracionInventada));
            LOGGER.info(String.format("Licencia prueba inventada con fecha: %s", claveGeneradaConFechaConMacInventada));
            claveGeneradaSinFechaConMacInventada = PasswordUtil.generatePassword(direccionMacInventada);
            LOGGER.info(String.format("Licencia prueba inventada sin fecha: %s", claveGeneradaSinFechaConMacInventada));

            claveGeneradaConFechaExpirada = PasswordUtil.generatePassword(direccion.concat(fechaExpiracionExpiradaInventada));
            LOGGER.info(String.format("Licencia prueba con fecha expirada: %s", claveGeneradaConFechaExpirada));
        } catch (GenerarClaveException e) {
            Assert.assertNotNull(e);
        }
    }

    @Test
    public void compararLicencia() {
        try {
            SeguridadesUtil.comprobarLicencia(claveGeneradaSinFecha);
            SeguridadesUtil.comprobarLicencia(claveGeneradaConFecha, fechaExpiracionInventada);
        } catch (NoLicenciaException | LicenciaExpiradaException | ErrorComparacionLicenciaException e) {
            Assert.fail();
        }
    }

    @Test
    public void compararLicenciaExpirada() {
        try {
            SeguridadesUtil.comprobarLicencia(claveGeneradaConFechaExpirada, fechaExpiracionExpiradaInventada);
            Assert.fail();
        } catch (NoLicenciaException | ErrorComparacionLicenciaException e) {
            Assert.fail();
        } catch (LicenciaExpiradaException e) {
            Assert.assertNotNull(e);
        }
    }

    @Test
    public void compararLicenciaConFechaMacNoValida() {
        try {
            SeguridadesUtil.comprobarLicencia(claveGeneradaConFechaConMacInventada, fechaExpiracionInventada);
            Assert.fail();
        } catch (LicenciaExpiradaException | ErrorComparacionLicenciaException e) {
            Assert.fail();
        } catch (NoLicenciaException e) {
            Assert.assertNotNull(e);
        }
    }

    @Test
    public void compararLicenciaSinFechaMacNoValida() {
        try {
            SeguridadesUtil.comprobarLicencia(claveGeneradaConFechaConMacInventada);
            Assert.fail();
        } catch (ErrorComparacionLicenciaException e) {
            Assert.fail();
        } catch (NoLicenciaException e) {
            Assert.assertNotNull(e);
        }
    }
}
