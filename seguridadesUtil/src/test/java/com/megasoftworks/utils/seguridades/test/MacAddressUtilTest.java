/**
 * Clase MacAddressUtilTest.java abr 03, 2019
 */
package com.megasoftworks.utils.seguridades.test;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.megasoftworks.utils.seguridades.MacAddressUtil;
import com.megasoftworks.utils.seguridades.beans.DireccionMac;

/**
 * @author pabloaleman
 */
public class MacAddressUtilTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(MacAddressUtilTest.class);

    @Test
    public void prueba() {
        List<DireccionMac> direccionesMac = MacAddressUtil.obtenerDireccionesMac();
        for (DireccionMac direccionMac : direccionesMac) {
            LOGGER.info(direccionMac.toString());
            Assert.assertNotNull(direccionesMac);
        }
    }


}
