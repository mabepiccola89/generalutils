/**
 * Clase MessageUtils.java mar 19, 2019
 */
package com.megasoftworks.utils.jsf;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

import com.megasoftworks.utils.jsf.enums.SeverityMessageEnum;

/**
 * @author pabloaleman
 */
public final class MessageUtils {
    private MessageUtils() {

    }

    public static void showMessage(String message, String detail) {
        showMessage(SeverityMessageEnum.INFO, message, detail);
    }

    public static void showMessage(SeverityMessageEnum type, String message, String detail) {

        Severity severity = FacesMessage.SEVERITY_INFO;
        if (type == SeverityMessageEnum.WARN) {
            severity = FacesMessage.SEVERITY_WARN;
        }

        if (type == SeverityMessageEnum.ERROR) {
            severity = FacesMessage.SEVERITY_ERROR;
        }
        showMessage(severity, message, detail);

    }

    public static void showMessage(Severity severity, String message, String detail) {
        if (message == null) {
            message = "";
        }
        if (detail == null) {
            detail = "";
        }
        FacesContext context = FacesContext.getCurrentInstance();
        FacesMessage fm = new FacesMessage(severity, message, detail);
        context.addMessage(null, fm);
    }

    public static void showMessages(List<String> mensajes, SeverityMessageEnum severity) {
        String message = "Info";
        if (severity == SeverityMessageEnum.ERROR) {
            message = "Error";
        } else if (severity == SeverityMessageEnum.WARN) {
            message = "Advertencia";
        }

        for (String mensaje : mensajes) {
            showMessage(severity, message, mensaje);
        }

    }

}
