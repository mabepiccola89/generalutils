/**
 * Clase SeverityMessageEnum.java mar 19, 2019
 */
package com.megasoftworks.utils.jsf.enums;

/**
 * @author pabloaleman
 */
public enum SeverityMessageEnum {
    INFO, WARN, ERROR
}
