package com.megasoftworks.utils.tipos.archivos.test;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.megasoftworks.utils.tipos.archivos.TiposArchivoEnum;
import com.megasoftworks.utils.tipos.archivos.exceptions.TipoArchivoNoEncontradoException;

public class TiposArchivoEnumTest {

    private static final String error = "Error en la prueba";
    private static final String cadena = "12345";

    @Rule
    public ExpectedException expectedExceptionRule = ExpectedException.none();

    @Test
    public void getTipoArchivoPorExtension() {
        try {
            TiposArchivoEnum retornado = TiposArchivoEnum.getTipoArchivoPorExtension("jpg");
            Assert.assertEquals(error, TiposArchivoEnum.JPEG, retornado);
        } catch (TipoArchivoNoEncontradoException e) {
            Assert.fail(error);
        }
    }

    @Test
    public void getTipoArchivoPorMimeType() {
        try {
            TiposArchivoEnum retornado = TiposArchivoEnum.getTipoArchivoPorMimeType("image/jpeg");
            Assert.assertEquals(error, TiposArchivoEnum.JPEG, retornado);
        } catch (TipoArchivoNoEncontradoException e) {
            Assert.fail(error);
        }

    }

    @Test
    public void getTipoArchivoPorExtensionNoEncontrado() {
        try {
            TiposArchivoEnum.getTipoArchivoPorExtension(cadena);
            Assert.fail(error);
        } catch (TipoArchivoNoEncontradoException e) {
            Assert.assertNotNull(e);
        }
    }

    @Test
    public void getTipoArchivoPorMimeTypeNoEncontrado() {
        try {
            TiposArchivoEnum.getTipoArchivoPorMimeType(cadena);
            Assert.fail(error);
        } catch (TipoArchivoNoEncontradoException e) {
            Assert.assertNotNull(e);
        }

    }

}
