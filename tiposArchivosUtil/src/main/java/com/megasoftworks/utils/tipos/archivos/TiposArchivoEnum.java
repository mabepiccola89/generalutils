/**
 * Clase TiposArchivoEnum.java mar 22, 2019
 */
package com.megasoftworks.utils.tipos.archivos;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.megasoftworks.utils.tipos.archivos.exceptions.TipoArchivoNoEncontradoException;

/**
 * @author pabloaleman
 */
public enum TiposArchivoEnum {

    PDF("application/pdf", Collections.singletonList("pdf")),
    JPEG("image/jpeg", Arrays.asList("jpeg", "jpg", "jpe")),
    PNG("image/png", Collections.singletonList("png")),
    XML("text/xml", Collections.singletonList("xml")),
    XLSX("application/vnd.ms-excel", Collections.singletonList("xls")),
    CSV("text/csv", Collections.singletonList("csv"));

    private final String mimeType;
    private final List<String> extensiones;

    TiposArchivoEnum(String mimeType, List<String> extensiones) {
        this.mimeType = mimeType;
        this.extensiones = extensiones;
    }

    public static TiposArchivoEnum getTipoArchivoPorExtension(String extension) throws TipoArchivoNoEncontradoException {
        for (TiposArchivoEnum tiposArchivoEnum : TiposArchivoEnum.values()) {
            if (tiposArchivoEnum.extensiones.contains(extension.toLowerCase())) {
                return tiposArchivoEnum;
            }
        }
        throw new TipoArchivoNoEncontradoException();
    }

    public static TiposArchivoEnum getTipoArchivoPorMimeType(String mimeType) throws TipoArchivoNoEncontradoException {
        for (TiposArchivoEnum tiposArchivoEnum : TiposArchivoEnum.values()) {
            if (mimeType.equalsIgnoreCase(tiposArchivoEnum.mimeType)) {
                return tiposArchivoEnum;
            }
        }
        throw new TipoArchivoNoEncontradoException();
    }
}
