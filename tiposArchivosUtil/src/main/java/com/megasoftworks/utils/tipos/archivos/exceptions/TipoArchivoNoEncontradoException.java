/**
 * Clase TipoArchivoNoEncontradoException.java mar 22, 2019
 */
package com.megasoftworks.utils.tipos.archivos.exceptions;

/**
 * @author pabloaleman
 */
public class TipoArchivoNoEncontradoException extends Exception {
    private static final long serialVersionUID = -5255186448934532936L;
}
