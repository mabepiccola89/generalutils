/**
 * Clase SHAHashingUtil.java mar 22, 2019
 */
package com.megasoftworks.utils.shas;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.megasoftworks.utils.shas.exceptions.EncriptaException;

/**
 * @author pabloaleman
 */
public final class SHAHashingUtil {
    static final Logger LOGGER = LoggerFactory.getLogger(SHAHashingUtil.class);

    private SHAHashingUtil() {
    }

    /**
     * Funcion que encripta una cadena.
     * @param dato la cadena a encriptar.
     * @return la cadena encriptada.
     * @throws EncriptaException en caso de errores en la encriptacion.
     */
    public static String encripta(String dato) throws EncriptaException {
        try {
            String password = dato;
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(password.getBytes());
            byte[] byteData = md.digest();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("Error encriptando", e);
            throw new EncriptaException(e);
        }
    }
}
