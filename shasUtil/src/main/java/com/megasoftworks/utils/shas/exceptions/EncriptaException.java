/**
 * Clase EncriptaException.java mar 22, 2019
 */
package com.megasoftworks.utils.shas.exceptions;

/**
 * @author pabloaleman
 */
public class EncriptaException extends Exception {

    private static final long serialVersionUID = 8105340939950468548L;

    public EncriptaException(Throwable t) {
        super(t);
    }
}
