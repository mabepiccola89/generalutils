/**
 * Clase SHAHashingUtilTest.java mar 27, 2019
 */
package com.megasoftworks.utils.shas.test;

import org.junit.Assert;
import org.junit.Test;

import com.megasoftworks.utils.shas.SHAHashingUtil;
import com.megasoftworks.utils.shas.exceptions.EncriptaException;

/**
 * @author pabloaleman
 */
public class SHAHashingUtilTest {
    private static final String cadenaCruda = "12345";
    private static final String cadenaEncriptada = "5994471ABB01112AFCC18159F6CC74B4F511B99806DA59B3CAF5A9C173CACFC5";
    private static final String mensajeError = "Error en la prueba";

    @Test
    public void encripta() {
        try {
            String retornado = SHAHashingUtil.encripta(cadenaCruda);
            Assert.assertEquals(mensajeError, cadenaEncriptada, retornado.toUpperCase());
        } catch (EncriptaException e) {
            Assert.fail(mensajeError);
        }
    }
}
