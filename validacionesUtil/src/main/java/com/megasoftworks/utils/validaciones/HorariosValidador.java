/**
 * Clase HorariosValidador.java mar 22, 2019
 */
package com.megasoftworks.utils.validaciones;

import com.megasoftworks.utils.fechas.FechasUtil;
import com.megasoftworks.utils.fechas.exceptions.ConversionFechaException;
import com.megasoftworks.utils.validaciones.beans.Horario;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author pabloaleman
 */
public final class HorariosValidador {
    private HorariosValidador() {
    }

    public static String validaHorarioAtencion(String texto, String separadorListaHorarios, String separadorHoras) {
        texto = texto.replace(" ", "");
        String[] arregloHorarios = texto.split(separadorListaHorarios);
        if (arregloHorarios.length > 0) {
            List<Horario> horarioList = new ArrayList<>();
            for (String horario : arregloHorarios) {
                String[] arregloHorario = horario.split(separadorHoras);
                if (arregloHorario.length == 2) {
                    try {
                        String[] horasMinutosInicio = arregloHorario[0].split(":");
                        String[] horasMinutosFin = arregloHorario[1].split(":");
                        if (horasMinutosInicio.length != 2 || horasMinutosFin.length != 2) {
                            return ConstantesValidaciones.ERROR_FORMATO_HORARIO;
                        }

                        int horaInicio = Integer.parseInt(horasMinutosInicio[0]);
                        int minutoInicio = Integer.parseInt(horasMinutosInicio[1]);
                        int horaFin = Integer.parseInt(horasMinutosFin[0]);
                        int minutoFin = Integer.parseInt(horasMinutosFin[1]);
                        if (!validaValorHora(horaInicio)
                                || !validaValorMinuto(minutoInicio)
                                || !validaValorHora(horaFin)
                                || !validaValorMinuto(minutoFin)) {
                            return ConstantesValidaciones.ERROR_FORMATO_HORARIO;
                        }

                        Date inicio = FechasUtil.stringToDate(arregloHorario[0], "HH:mm");
                        Date fin = FechasUtil.stringToDate(arregloHorario[1], "HH:mm");

                        if (inicio.after(fin)) {
                            return ConstantesValidaciones.ERROR_DIFERENCIA_HORARIO;
                        }
                        if (!horarioList.isEmpty()) {
                            for (Horario horarioAux : horarioList) {
                                if (horarioAux.intermedio(inicio) || horarioAux.intermedio(fin)) {
                                    return ConstantesValidaciones.ERROR_CRUCE_HORARIOS;
                                }
                            }
                        }

                        horarioList.add(new Horario(inicio, fin));
                    } catch (NumberFormatException e) {
                        return ConstantesValidaciones.ERROR_FORMATO_HORARIO;
                    } catch (ConversionFechaException e) {
                        return ConstantesValidaciones.ERROR_FORMATO_HORARIO;
                    }
                } else {
                    return ConstantesValidaciones.ERROR_FORMATO_HORARIO;
                }

            }
            return null;
        } else {
            return ConstantesValidaciones.ERROR_FORMATO_HORARIO;
        }
    }

    public static String validaIntervalosTiempo(String cadena, String formatoFecha, String separadorFechas) {
        String[] fechas = cadena.split(separadorFechas);
        if (fechas.length != 2) {
            return ConstantesValidaciones.ERROR_FORMATO_INTERVALO;
        }
        Date fechaInicio;
        Date fechaFin;
        try {
            fechaInicio = FechasUtil.stringToDate(fechas[0], formatoFecha);
        } catch (ConversionFechaException e) {
            return ConstantesValidaciones.ERROR_FECHA_INICIO_INTERVALO;
        }
        try {
            fechaFin = FechasUtil.stringToDate(fechas[1], formatoFecha);
        } catch (ConversionFechaException e) {
            return ConstantesValidaciones.ERROR_FECHA_FIN_INTERVALO;
        }

        if (fechaFin.getTime() < fechaInicio.getTime()) {
            return ConstantesValidaciones.ERROR_FECHA_INICIO_MAYOR_A_FECHA_FIN_INTERVALO;
        }
        return null;
    }

    private static boolean validaValorHora(int hora) {
        return hora >= 0 && hora <= 23;
    }

    private static boolean validaValorMinuto(int minuto) {
        return minuto >= 0 && minuto <= 59;
    }
}
