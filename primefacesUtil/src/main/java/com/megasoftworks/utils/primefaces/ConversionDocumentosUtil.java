/**
 * Clase ConversionDocumentosUtil.java mar 19, 2019
 */
package com.megasoftworks.utils.primefaces;

import org.apache.commons.codec.binary.Base64;
import org.primefaces.model.ByteArrayContent;
import org.primefaces.model.StreamedContent;

import com.megasoftworks.utils.primefaces.model.DefaultStreamedContentSerializado;

/**
 * @author pabloaleman
 */
public final class ConversionDocumentosUtil {

    private ConversionDocumentosUtil() {

    }

    public static StreamedContent byteAToStreamedContent(byte[] arreglo) {
        java.io.InputStream is = new java.io.ByteArrayInputStream(arreglo);
        return new DefaultStreamedContentSerializado(is);
    }

    public static StreamedContent byteAToStreamedContent(byte[] arreglo, String tipoArchivo, String nombre) {
        return new ByteArrayContent(arreglo, tipoArchivo, nombre);
    }

    public static String byteAToStringBase64(byte[] arreglo) {
        return new String(Base64.encodeBase64(arreglo));
    }
}
