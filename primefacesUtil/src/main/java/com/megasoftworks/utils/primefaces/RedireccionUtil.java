/**
 * Clase RedireccionUtil.java mar 19, 2019
 */
package com.megasoftworks.utils.primefaces;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author pabloaleman
 */
public final class RedireccionUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(RedireccionUtil.class);

    private RedireccionUtil() {

    }

    public static void redireccionarHacia(String ruta, boolean keepMessages) {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        try {
            context.redirect(context.getRequestContextPath() + ruta);
			context.getFlash().setKeepMessages(keepMessages);
			update();
        } catch (IOException e) {
            LOGGER.error("Problema en la redireccion, clase: RedireccionUtil", e);
        }
    }

    public static void redireccionarHacia(String ruta) {
        redireccionarHacia(ruta, false);
    }

    public static void update(String id) {
        RequestContext rc = RequestContext.getCurrentInstance();
        rc.update(id);
    }

    public static void update() {
        RequestContext rc = RequestContext.getCurrentInstance();
        rc.update("@all");
    }

    public static void update(String... params) {
        RequestContext context = RequestContext.getCurrentInstance();
        List<String> updates = new ArrayList<>();
        Collections.addAll(updates, params);
        context.update(updates);
    }

    public static void execute(String comando) {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute(comando);
    }

    public static void returnObject(String mimeType, String nombreArchivo, byte[] contenido) {
        try {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ExternalContext externalContext = facesContext.getExternalContext();
            HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
            response.reset();
            response.setContentType(mimeType);
            response.setHeader("Content-disposition", "attachment; filename=\"" + nombreArchivo + "\"");
            OutputStream output = response.getOutputStream();
            output.write(contenido);
            output.close();
            facesContext.responseComplete();
        } catch (IOException e) {
            LOGGER.error("Error tratando archivo para response", e);
        }
    }
}
