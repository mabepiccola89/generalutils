/**
 * Clase ConversionArchivosException.java mar 22, 2019
 */
package com.megasoftworks.utils.files.exceptions;

/**
 * @author pabloaleman
 */
public class ConversionArchivosException extends Exception {
    private static final long serialVersionUID = -7856566019666126749L;

    public ConversionArchivosException(Throwable t) {
        super(t);
    }
}
