/**
 * Clase FilesUtil.java mar 22, 2019
 */
package com.megasoftworks.utils.files;

import com.megasoftworks.utils.files.exceptions.ConversionArchivosException;
import com.megasoftworks.utils.files.exceptions.CrearArchivoException;
import com.megasoftworks.utils.files.exceptions.LeerArchivoException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

/**
 * @author pabloaleman
 */
public final class FilesUtil {

    static final Logger LOGGER = LoggerFactory.getLogger(FilesUtil.class);

    private FilesUtil() {
    }

    /**
     * Funcion que crea un archivo desde un byte array.
     *
     * @param contenido  el contenido a escribir.
     * @param rutaSalida la ruta de salida del archivo.
     * @throws CrearArchivoException en caso de error.
     */
    public static void crearArchivoDesdeBA(byte[] contenido, String rutaSalida) throws CrearArchivoException {
        try (InputStream is = new ByteArrayInputStream(contenido);
             DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(new File(rutaSalida))))) {
            int c;
            while ((c = is.read()) != -1) {
                out.writeByte(c);
            }
        } catch (IOException e) {
            LOGGER.error("Error creando archivo desde binario ", e);
            throw new CrearArchivoException(e);
        }
    }

    /**
     * Funcion que escribe un contenido a un archivo.
     *
     * @param path      el path donde se va a grabar el archivo.
     * @param contenido el contenido a escribir.
     * @throws CrearArchivoException en caso de error.
     */
    public static void escribirArchivo(String path, String contenido) throws CrearArchivoException {
        try (FileWriter salida = new FileWriter(path)) {
            salida.write(contenido);
        } catch (IOException ex) {
            LOGGER.error("Error creando archivo desde binario", ex);
            throw new CrearArchivoException(ex);
        }
    }


    /**
     * Funcion que retorna un string que representa el contenido de un archivo plano.
     *
     * @param path el path donde se encuentra el archivo.
     * @return el contenido del archivo.
     * @throws LeerArchivoException en caso de errores.
     */
    public static String leerArchivo(String path) throws LeerArchivoException {
        File f = new File(path);
        try (BufferedReader entrada = new BufferedReader(new FileReader(f))) {
            StringBuilder lectura = new StringBuilder();
            if (f.exists()) {
                while (entrada.ready()) {
                    String linea = entrada.readLine() + "\n";
                    lectura.append(linea);
                }
                return lectura.toString();
            } else {
                String mensaje = String.format("El archivo: %s no existe", path);
                LOGGER.error(mensaje);
                throw new LeerArchivoException(new Exception(mensaje));
            }
        } catch (IOException ex) {
            LOGGER.error("Error leyendo el archivo", ex);
            throw new LeerArchivoException(ex);
        }
    }

    /**
     * Funcion que transforma un outputstream a byte array.
     *
     * @param outputStream el outputstream a convertir.
     * @return el arreglo de bytes.
     */
    public static byte[] outputStreamToByteArray(OutputStream outputStream) {
        ByteArrayOutputStream baos = (ByteArrayOutputStream) outputStream;
        return baos.toByteArray();
    }

    /**
     * Funcion que convierte un inputstream a string.
     *
     * @param is el inputstream a convertir.
     * @return el string resultado.
     * @throws ConversionArchivosException en caso de errores.
     */
    public static String inputStreamToString(InputStream is) throws ConversionArchivosException {
        try {
            StringWriter sw = new StringWriter();
            IOUtils.copy(is, sw);
            return sw.toString();
        } catch (IOException e) {
            LOGGER.error("Error convirtiendo el archivo", e);
            throw new ConversionArchivosException(e);
        }
    }

    /**
     * Funcion que convierte un string a Byte array base 64.
     *
     * @param contenido el String a convertir.
     * @return el Byte array base 64 resultado.
     * @throws ConversionArchivosException en caso de errores.
     */
    public static byte[] stringToBAB64(String contenido) throws ConversionArchivosException {
        try {
            byte[] name = Base64.encodeBase64(contenido.getBytes());
            return Base64.decodeBase64(new String(name).getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("Error cambiando de formato string a base64", e);
            throw new ConversionArchivosException(e);
        }
    }

    /**
     * Funcion que valida si existe un archivo.
     *
     * @param path el path del archivo a evaluar.
     * @return si existe o no.
     */
    public static boolean existeArchivo(String path) {
        File archivo = new File(path);
        return archivo.exists() && archivo.canRead() && !archivo.isDirectory();
    }
}
