/**
 * Clase CreacionZipException.java mar 25, 2019
 */
package com.megasoftworks.utils.zip.exceptions;

/**
 * @author pabloaleman
 */
public class CreacionZipException extends Exception {
    private static final long serialVersionUID = 2402240732145646245L;

    public CreacionZipException(Throwable t) {
        super(t);
    }
}
