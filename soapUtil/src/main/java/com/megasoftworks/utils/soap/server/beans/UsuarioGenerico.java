/**
 * Clase UsuarioGenerico.java mar 19, 2019
 */
package com.megasoftworks.utils.soap.server.beans;

/**
 * @author pabloaleman
 */
public class UsuarioGenerico {
    private int idUsuario;
    private String nui;
    private String username;
    private String password;

    public UsuarioGenerico(String us, String pass) {
        this.username = us;
        this.password = pass;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNui() {
        return nui;
    }

    public void setNui(String nui) {
        this.nui = nui;
    }
}
