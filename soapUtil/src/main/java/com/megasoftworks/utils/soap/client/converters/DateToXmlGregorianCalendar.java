/**
 * Clase DateToXmlGregorianCalendar.java mar 19, 2019
 */
package com.megasoftworks.utils.soap.client.converters;

import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.megasoftworks.utils.soap.client.exceptions.ConverterException;

/**
 * @author pabloaleman
 */
public final class DateToXmlGregorianCalendar {
    private DateToXmlGregorianCalendar() {
    }

    public static XMLGregorianCalendar convert(Date fecha) throws ConverterException {
        try {
            GregorianCalendar gregorianCalendar = new GregorianCalendar();
            gregorianCalendar.setTime(fecha);
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
        } catch (Exception e) {
            throw new ConverterException(e);
        }
    }
}
