/**
 * Clase ObtenerDatosUsuarioUtil.java mar 19, 2019
 */
package com.megasoftworks.utils.soap.server;

import java.util.ArrayList;
import java.util.Map;

import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import org.apache.commons.codec.binary.Base64;

import com.megasoftworks.utils.soap.server.beans.UsuarioGenerico;
import com.megasoftworks.utils.soap.server.exceptions.ErrorObteniendoInformacionUsuarioException;
import com.megasoftworks.utils.soap.server.exceptions.NoHayInformacionEnBasicAuthException;

/**
 * @author pabloaleman
 */
public final class ObtenerDatosUsuarioUtil {
    private ObtenerDatosUsuarioUtil() {
    }

    public static UsuarioGenerico getBasicAuthenticationData(WebServiceContext wsctx) throws
            ErrorObteniendoInformacionUsuarioException, NoHayInformacionEnBasicAuthException {

        MessageContext mctx = wsctx.getMessageContext();
        Map<?, ?> httpHeaders = (Map<?, ?>) mctx.get(MessageContext.HTTP_REQUEST_HEADERS);

        ArrayList<?> list = (ArrayList<?>) httpHeaders.get("Authorization");
        if (list == null || list.isEmpty()) {
            throw new NoHayInformacionEnBasicAuthException();
        }

        String userpass = (String) list.get(0);
        userpass = userpass.substring(5);
        byte[] buf = Base64.decodeBase64(userpass.getBytes());
        String credentials = new String(buf);

        int p = credentials.indexOf(':');
        if (p > -1) {
            return new UsuarioGenerico(credentials.substring(0, p), credentials.substring(p + 1));
        } else {
            throw new ErrorObteniendoInformacionUsuarioException();
        }
    }

}
