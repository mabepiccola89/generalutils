/**
 * Clase NoHayInformacionEnBasicAuth.java mar 22, 2019
 */
package com.megasoftworks.utils.soap.server.exceptions;

/**
 * @author pabloaleman
 */
public class NoHayInformacionEnBasicAuthException extends Exception {
    private static final long serialVersionUID = -3109527821369880342L;

    public NoHayInformacionEnBasicAuthException() {
        super("Authentication failed! This WS needs BASIC Authentication!");
    }
}
