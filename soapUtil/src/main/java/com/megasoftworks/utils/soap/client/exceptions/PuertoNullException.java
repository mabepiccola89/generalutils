/**
 * Clase PuertoNullException.java mar 19, 2019
 */
package com.megasoftworks.utils.soap.client.exceptions;

/**
 * @author pabloaleman
 */
public class PuertoNullException extends Exception {

    private static final long serialVersionUID = -4888642769979841621L;

    public PuertoNullException(String m) {
        super(m);
    }
}
