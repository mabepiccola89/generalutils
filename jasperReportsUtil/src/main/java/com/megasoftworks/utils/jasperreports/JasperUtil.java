/**
 * Clase JasperUtil.java mar 19, 2019
 */
package com.megasoftworks.utils.jasperreports;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.util.Map;

import com.megasoftworks.utils.files.FilesUtil;
import org.apache.commons.io.IOUtils;


import com.megasoftworks.utils.jasperreports.enums.JasperUtilFaseEnum;
import com.megasoftworks.utils.jasperreports.exceptions.JasperUtilException;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

/**
 * @author pabloaleman
 */
public final class JasperUtil {
    private JasperUtil() {
    }

    /**
     * Funcion que retorna un pdf de un reporte dado.
     *
     * @param jrxmlFileContent el contenido del archivo jrxml.
     * @param params           los parametros del reporte.
     * @param con              la conexion a la base de datos (null) si no es con conexion a base de datos.
     * @throws JasperUtilException en caso de errores.
     * @returnel byte array del reporte en formato pdf.
     */
    public static byte[] generatePdfReport(String jrxmlFileContent, Map<String, Object> params, Connection con) throws JasperUtilException {
        JasperReport jasperReport = compileJasper(jrxmlFileContent);
        JasperPrint jasperPrint = generatePrint(jasperReport, params, con);
        return generatePdfByteArray(jasperPrint);

    }

    /**
     * Funcion que retorna un byte array de un reporte dado en formato excel.
     *
     * @param jrxmlFileContent el contenido del archivo jrxml.
     * @param params           los parametros del reporte.
     * @param con              la conexion a la base de datos (null) si no es con conexion a base de datos.
     * @return el byte array del reporte en formato excel.
     * @throws JasperUtilException en caso de errores.
     */
    public static byte[] generateExcelReport(String jrxmlFileContent, Map<String, Object> params, Connection con) throws JasperUtilException {
        JasperReport jasperReport = compileJasper(jrxmlFileContent);
        JasperPrint jasperPrint = generatePrint(jasperReport, params, con);
        return generateExcelByteArray(jasperPrint);
    }

    /**
     * Funcion que compila el contenido de un reporte en formato jrxml para convertir un objeto Jasper.
     *
     * @param content el contenido del archivo jrxml.
     * @return el reporte en formato {@link JasperReport}
     * @throws JasperUtilException en caso de errores en la compilacion.
     */
    public static JasperReport compileJasper(String content) throws JasperUtilException {
        try {
            return JasperCompileManager.compileReport(IOUtils.toInputStream(content));
        } catch (Exception e) {
            throw new JasperUtilException(e.getMessage(), JasperUtilFaseEnum.COMPILACION);
        }
    }

    /**
     * Funcion que obtiene un objeto tipo {@link JasperPrint}
     *
     * @param jasperReport el reporte en formato {@link JasperReport}
     * @param params       los parametros del reporte.
     * @param con          la conexion a la base de datos (null) si no es con conexion a base de datos.
     * @throws JasperUtilException en caso de errores.
     * @returnel objeto en formato {@link JasperPrint}
     */
    public static JasperPrint generatePrint(JasperReport jasperReport, Map<String, Object> params, Connection con) throws JasperUtilException {
        try {
            if (con == null) {
                return JasperFillManager.fillReport(jasperReport, params, new JREmptyDataSource());
            } else {
                return JasperFillManager.fillReport(jasperReport, params, con);
            }
        } catch (Exception e) {
            throw new JasperUtilException(e.getMessage(), JasperUtilFaseEnum.GENERATE_PRINT);
        }
    }

    /**
     * Funcion que recibe un reporte en formato {@link JasperPrint} y lo convierte a un byte array que representa un pdf.
     *
     * @param jasperPrint el objeto a convertir.
     * @return el byte array del reporte en formato pdf.
     * @throws JasperUtilException en caso de errores.
     */
    public static byte[] generatePdfByteArray(JasperPrint jasperPrint) throws JasperUtilException {
        try {
            return JasperExportManager.exportReportToPdf(jasperPrint);
        } catch (Exception e) {
            throw new JasperUtilException(e.getMessage(), JasperUtilFaseEnum.GENERATE_PDF_BYTE_ARRAY);
        }
    }

    /**
     * Funcion que recibe un reporte en formato {@link JasperPrint} y lo convierte a un byte array que representa un excel.
     *
     * @param jasperPrint el objeto a convertir.
     * @return el byte array del reporte en formato excel.
     * @throws JasperUtilException en caso de errores.
     */
    public static byte[] generateExcelByteArray(JasperPrint jasperPrint) throws JasperUtilException {
        try {
            OutputStream outputStream = new ByteArrayOutputStream();
            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));
            exporter.exportReport();
            return FilesUtil.outputStreamToByteArray(outputStream);
        } catch (Exception e) {
            throw new JasperUtilException(e.getMessage(), JasperUtilFaseEnum.GENERATE_PDF_BYTE_ARRAY);
        }
    }
}
