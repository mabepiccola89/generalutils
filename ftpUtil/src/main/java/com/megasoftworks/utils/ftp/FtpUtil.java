/**
 * Clase FtpUtil.java mar 19, 2019
 */
package com.megasoftworks.utils.ftp;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author pabloaleman
 */
public class FtpUtil {
    static final Logger LOGGER = LoggerFactory.getLogger(FtpUtil.class);

    /**
     * Funcion que sube un archivo a un repositorio FTP.
     *
     * @param ftpServer     IP o URL del servidor FTP.
     * @param ftpPort       Puerto para conexión.
     * @param ftpUser       el nombre de usuario para conexión.
     * @param ftpPass       el password para conexión.
     * @param directorio    el directorio en el servidor.
     * @param nombreArchivo el nombre del archivo con el cual se va a guardar en el repositorio.
     * @param inputStream   el contenido del archivo.
     */
    public static void uploadFTP(String ftpServer, int ftpPort, String ftpUser, String ftpPass,
                                 File directorio, String nombreArchivo, InputStream inputStream) {

        Boolean conexion = false;
        FTPClient ftpClient = new FTPClient();
        try {

            ftpClient.connect(ftpServer, ftpPort);
            conexion = ftpClient.login(ftpUser, ftpPass);
            if (conexion) {
                LOGGER.info("Conexion satisfactoria.");

                ftpClient.enterLocalPassiveMode();
                ftpClient.setFileType(FTP.BINARY_FILE_TYPE);


                LOGGER.info("Inicio subida archivo");
                LOGGER.info("Directorio: {}", directorio.getAbsolutePath());
                Boolean existeDirectorio = ftpClient.changeWorkingDirectory(directorio.getAbsolutePath());
                LOGGER.info("ExisteDirectorio: {}", existeDirectorio);
                String[] dirArreglo = directorio.getAbsolutePath().split(File.separator);
                if (!existeDirectorio) {
                    StringBuilder direc = new StringBuilder();
                    for (String dir : dirArreglo) {
                        LOGGER.debug("dir: {}", dir);
                        if (dir != null && !dir.isEmpty()) {
                            direc.append(File.separator).append(dir);
                            ftpClient.makeDirectory(direc.toString());
                        }
                    }
                    LOGGER.info("Directorio nuevo: {}", direc);
                }
                boolean done = ftpClient.storeFile(directorio.getAbsolutePath() + File.separator + nombreArchivo, inputStream);
                inputStream.close();
                if (done) {
                    LOGGER.info("Archivo subido exitosamente");
                } else {
                    LOGGER.error("No subió el archivo FTP");
                }
            } else {
                LOGGER.error("No se realiza la Conexión.");
            }

        } catch (IOException ex) {
            LOGGER.error("Error: {}", ex.getMessage());
        } finally {
            desconectarServidor(ftpClient);
        }
    }

    /**
     * Funcion que desconecta del servidor.
     *
     * @param ftpClient el cliente a cerrar conexion.
     */
    private static void desconectarServidor(FTPClient ftpClient) {
        try {
            if (ftpClient.isConnected()) {
                ftpClient.logout();
                ftpClient.disconnect();
            }
        } catch (IOException ex) {
            LOGGER.error(ex.getMessage());
        }
    }

    /**
     * Funcion que sube un archivo a un repositorio FTP.
     *
     * @param ftpServer     IP o URL del servidor FTP.
     * @param ftpPort       Puerto para conexión.
     * @param ftpUser       el nombre de usuario para conexión.
     * @param ftpPass       el password para conexión.
     * @param urlArchivo    URL del archivo que se quiere descargar.
     * @param nombreArchivo El nombre del archivo a descargar.
     */

    public void downloadFTP(String ftpServer, int ftpPort, String ftpUser, String ftpPass, String urlArchivo,
                            String nombreArchivo) {
        FTPClient ftpClient = new FTPClient();
        Boolean conexionD = false;
        try {
            ftpClient.connect(ftpServer, ftpPort);
            conexionD = ftpClient.login(ftpUser, ftpPass);
            if (conexionD) {
                LOGGER.debug("Conexion satisfactoria.");
                ftpClient.enterLocalPassiveMode();
                ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

                String remoteFile = urlArchivo;
                File downloadFile = new File("/tmp/" + nombreArchivo);
                OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(downloadFile));
                boolean success = ftpClient.retrieveFile(remoteFile, outputStream);
                outputStream.close();

                if (success) {
                    LOGGER.info("Archivo descargado satisfactoriamente.");
                } else {
                    LOGGER.info("No existe archivo en Servidor FTP.");
                }
            } else {
                LOGGER.error("No se realiza la Conexión.");
            }
        } catch (IOException ex) {
            LOGGER.error("Error: {}", ex.getMessage());
        } finally {
            desconectarServidor(ftpClient);
        }
    }
}