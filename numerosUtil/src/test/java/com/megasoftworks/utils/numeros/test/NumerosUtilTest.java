/**
 * Clase NumerosUtilTest.java mar 27, 2019
 */
package com.megasoftworks.utils.numeros.test;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;

import com.megasoftworks.utils.numeros.NumerosUtil;
import com.megasoftworks.utils.numeros.exceptions.ConversionNumerosException;

/**
 * @author pabloaleman
 */
public class NumerosUtilTest {

    private static final String mensajeError = "Error en la prueba";

    @Test
    public void getNumeroFormateado() {
        try {
            Double numero = 5.6;
            String formato = "#.#";
            int nDecimales = 3;
            String retorno = NumerosUtil.getNumeroFormateado(numero, formato, nDecimales);
            Assert.assertEquals(mensajeError, "5.600", retorno);
        } catch (ConversionNumerosException e) {
            Assert.fail(mensajeError);
        }
    }

    @Test
    public void getNumeroFormateadoBigDecimal() {
        try {
            BigDecimal numero = new BigDecimal(5.6);
            String formato = "#.#";
            int nDecimales = 3;
            String retorno = NumerosUtil.getNumeroFormateado(numero, formato, nDecimales);
            Assert.assertEquals(mensajeError, "5.600", retorno);
        } catch (ConversionNumerosException e) {
            Assert.fail(mensajeError);
        }
    }

    @Test
    public void isInteger() {
        Assert.assertTrue(mensajeError, NumerosUtil.isInteger("1"));
        Assert.assertFalse(mensajeError, NumerosUtil.isInteger("112s"));
        Assert.assertFalse(mensajeError, NumerosUtil.isInteger(null));
    }

    @Test
    public void isDouble() {
        Assert.assertTrue(mensajeError, NumerosUtil.isDouble("1.1"));
        Assert.assertFalse(mensajeError, NumerosUtil.isDouble("112s"));
        Assert.assertFalse(mensajeError, NumerosUtil.isDouble(null));
    }

}
