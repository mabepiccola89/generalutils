/**
 * Clase ConversionNumerosException.java mar 22, 2019
 */
package com.megasoftworks.utils.numeros.exceptions;

/**
 * @author pabloaleman
 */
public class ConversionNumerosException extends Exception {
    private static final long serialVersionUID = 7267150482242264964L;

    public ConversionNumerosException(String mensaje, Throwable t) {
        super(mensaje, t);
    }
}
