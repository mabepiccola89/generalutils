/**
 * Clase ParamsReader.java abr 01, 2019
 */
package com.megasoftworks.utils.propiedades;

import com.megasoftworks.utils.files.FilesUtil;
import com.megasoftworks.utils.files.exceptions.LeerArchivoException;
import com.megasoftworks.utils.propiedades.exceptions.FormatoParametroException;
import com.megasoftworks.utils.propiedades.exceptions.ParametroNoEncontradoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author pabloaleman
 */
public class ParamsReader {
    private static final Logger logger = LoggerFactory.getLogger(ParamsReader.class);
    private static final String PARAMETRO_NO_EXISTE = "Parametro {} NO existe";
    private static final String PARAMETRO_VALOR = "{} = {}";

    private final  String separadorIgual;

    private final Map<String, String> params = new ConcurrentHashMap<>();


    public ParamsReader(String path, String separadorIgua) {
        separadorIgual = separadorIgua;
        readParams(path);
    }

    private void readParams(String path) {
        try {
            String archivoParams = FilesUtil.leerArchivo(path);
            String[] lineas = archivoParams.split("\n");
            if (lineas.length > 1) {
                for (String linea : lineas) {
                    if (!linea.startsWith("#") && linea.length() > 2) {
                        String[] datos = linea.split(separadorIgual);
                        if (datos.length == 2) {
                            params.put(datos[0], datos[1]);
                        } else {
                            logger.error("Linea {} no cumple con el formato ATRIBUTO=VALOR", linea);
                        }
                    }
                }
            } else {
                logger.error("El archivo {} no tiene parametros...", path);
            }
        } catch (LeerArchivoException e) {
            logger.error("Error leyendo el archivo de propiedades", e);
        }

    }

    public int getInt(String paramName) throws ParametroNoEncontradoException, FormatoParametroException {
        try {
            if (params.containsKey(paramName)) {
                int retorno = Integer.parseInt(params.get(paramName));
                logger.info(PARAMETRO_VALOR, paramName, retorno);
                return retorno;
            } else {
                logger.error(PARAMETRO_NO_EXISTE, paramName);
                throw new ParametroNoEncontradoException();
            }
        } catch (NumberFormatException e) {
            throw new FormatoParametroException();
        }

    }

    public String get(String paramName) throws ParametroNoEncontradoException {
        if (params.containsKey(paramName)) {
            String retorno = params.get(paramName);
            logger.info(PARAMETRO_VALOR, paramName, retorno);
            return retorno;
        } else {
            logger.error(PARAMETRO_NO_EXISTE, paramName);
            throw new ParametroNoEncontradoException();
        }

    }

    public String getNoLog(String paramName) throws ParametroNoEncontradoException {
        if (params.containsKey(paramName)) {
            return params.get(paramName);
        } else {
            logger.error(PARAMETRO_NO_EXISTE, paramName);
            throw new ParametroNoEncontradoException();
        }

    }

    public Double getDouble(String paramName) throws ParametroNoEncontradoException, FormatoParametroException {
        try {
            if (params.containsKey(paramName)) {
                double retorno = Double.parseDouble(params.get(paramName));
                logger.info(PARAMETRO_VALOR, paramName, retorno);
                return retorno;
            } else {
                logger.error(PARAMETRO_NO_EXISTE, paramName);
                throw new ParametroNoEncontradoException();
            }
        } catch (NumberFormatException e) {
            throw new FormatoParametroException();
        }
    }

    public boolean getBoolean(String paramName) throws ParametroNoEncontradoException, FormatoParametroException {
        if (params.containsKey(paramName)) {
            if (params.get(paramName).equalsIgnoreCase("true") || params.get(paramName).equalsIgnoreCase("false")) {
                boolean retorno = params.get(paramName).equalsIgnoreCase("true");
                logger.info(PARAMETRO_VALOR, paramName, retorno);
                return retorno;
            } else {
                throw new FormatoParametroException();
            }

        } else {
            logger.error(PARAMETRO_NO_EXISTE, paramName);
            throw new ParametroNoEncontradoException();
        }
    }

    public List<String> getArrayList(String paramName, String separator) throws ParametroNoEncontradoException {
        if (params.containsKey(paramName)) {
            ArrayList<String> retorno = new ArrayList<>();
            String[] datos = params.get(paramName).split(separator);
            retorno.addAll(Arrays.asList(datos));
            logger.info(PARAMETRO_VALOR, paramName, retorno);
            return retorno;
        } else {
            logger.error(PARAMETRO_NO_EXISTE, paramName);
            throw new ParametroNoEncontradoException();
        }

    }
}
