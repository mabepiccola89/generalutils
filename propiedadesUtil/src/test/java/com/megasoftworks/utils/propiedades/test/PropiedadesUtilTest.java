/**
 * Clase PropiedadesUtilTest.java abr 01, 2019
 */
package com.megasoftworks.utils.propiedades.test;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.megasoftworks.utils.propiedades.ParamsReader;
import com.megasoftworks.utils.propiedades.exceptions.FormatoParametroException;
import com.megasoftworks.utils.propiedades.exceptions.ParametroNoEncontradoException;

/**
 * @author pabloaleman
 */
public class PropiedadesUtilTest {
    private static final String pathDat = "src/test/resources/parametros.dat";
    private static final int numerico = 1;
    private static final String nombeNumerico = "numerico";
    private static final double dobleSinPunto = 2.0;
    private static final String nombreDobleSinPunto = "dobleSinPunto";
    private static final double doble = 2.0;
    private static final String nombreDoble = "doble";
    private static final String cadena = "Hola como vas";
    private static final String nombreCadena = "cadena";
    private static final List<String> listaEsperada = Arrays.asList("uno", "dos", "tres");
    private static final String nombreLista = "list";
    private static final String nombreNoExiste = "lists";
    private static final double DELTA = 1e-15;

    @Test
    public void paramsReaderTest() {
        ParamsReader paramsReader = new ParamsReader(pathDat, "=");
        try {
            Assert.assertEquals(numerico, paramsReader.getInt(nombeNumerico));
            Assert.assertEquals(dobleSinPunto, paramsReader.getDouble(nombreDobleSinPunto), DELTA);
            Assert.assertEquals(doble, paramsReader.getDouble(nombreDoble), DELTA);
            Assert.assertEquals(cadena, paramsReader.get(nombreCadena));
            Assert.assertEquals(listaEsperada, paramsReader.getArrayList(nombreLista, ","));
            Assert.assertTrue(paramsReader.getBoolean("boolean"));
            Assert.assertTrue(paramsReader.getBoolean("boolean2"));
            Assert.assertFalse(paramsReader.getBoolean("booleanFalse"));
            Assert.assertFalse(paramsReader.getBoolean("booleanFalse2"));
        } catch (ParametroNoEncontradoException | FormatoParametroException e) {
            Assert.fail();
        }

        try {
            paramsReader.getDouble(nombreNoExiste);
            Assert.fail();
        } catch (ParametroNoEncontradoException e) {
            Assert.assertNotNull(e);
        } catch (FormatoParametroException e) {
            Assert.fail();
        }

        try {
            paramsReader.getInt(nombreNoExiste);
            Assert.fail();
        } catch (ParametroNoEncontradoException e) {
            Assert.assertNotNull(e);
        } catch (FormatoParametroException e) {
            Assert.fail();
        }

        try {
            paramsReader.get(nombreNoExiste);
            Assert.fail();
        } catch (ParametroNoEncontradoException e) {
            Assert.assertNotNull(e);
        }

        try {
            paramsReader.getArrayList(nombreNoExiste, ",");
            Assert.fail();
        } catch (ParametroNoEncontradoException e) {
            Assert.assertNotNull(e);
        }

        try {
            paramsReader.getDouble(nombreCadena);
            Assert.fail();
        } catch (ParametroNoEncontradoException e) {
            Assert.fail();
        } catch (FormatoParametroException e) {
            Assert.assertNotNull(e);
        }

        try {
            paramsReader.getInt(nombreCadena);
            Assert.fail();
        } catch (ParametroNoEncontradoException e) {
            Assert.fail();
        } catch (FormatoParametroException e) {
            Assert.assertNotNull(e);
        }
    }
}
