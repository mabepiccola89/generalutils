/**
 * Clase CedulaRucValidador.java mar 19, 2019
 */
package com.megasoftworks.utils.jsf.validadores;

import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import com.megasoftworks.utils.validaciones.Validaciones;

/**
 * @author pabloaleman
 */
@FacesValidator("CedulaRucValidador")
public class CedulaRucValidador implements Validator {
    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String resultadoValidacion = Validaciones.validaCedulaRuc(value.toString());
        if (resultadoValidacion != null) {
            String mensaje = ResourceBundle.getBundle("validaciones").getString("cedulaRuc." + resultadoValidacion);
            FacesMessage msg =
                    new FacesMessage(mensaje,
                            mensaje);
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);

        }

    }

}
