package com.megasoftworks.utils.fechas.exceptions;

public class ConversionFechaException extends Exception {

    private static final long serialVersionUID = -466715296039440325L;

    public ConversionFechaException(String m, Throwable e) {
        super(m, e);
    }
}
