/**
 * Clase AverageListaNullVaciaException.java abr 01, 2019
 */
package com.megasoftworks.utils.promedios.exceptions;

/**
 * @author pabloaleman
 */
public class AverageListaNullVaciaException extends Exception {
    private static final long serialVersionUID = -3817205788762472177L;
}
