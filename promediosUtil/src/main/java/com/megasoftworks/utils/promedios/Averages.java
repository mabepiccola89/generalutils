/**
 * Clase Averages.java abr 01, 2019
 */
package com.megasoftworks.utils.promedios;

import com.megasoftworks.utils.promedios.exceptions.AverageListaNullVaciaException;

import java.util.List;

/**
 * @author pabloaleman
 */
public class Averages {

    /**
     * Función que calcula el promedio logaritmico de un listado de datos que implementan la interfaz {@link Promediable}
     *
     * @param datos los datos a calcular.
     * @param <T>   El tipo de valores.
     * @return el promedio logaritmico de los datos dados.
     */
    public static <T extends Promediable> Double getLogarithmicAverage(List<T> datos) throws AverageListaNullVaciaException {
        comprobarListaNoVaciaNula(datos);
        double valor = 0.0;
        for (Promediable promediable : datos) {
            Double exp = promediable.getToAvgValue() / 10.0;
            double suma = Math.pow(10, exp);
            valor = valor + suma;
        }
        valor = valor * 1.0 / Double.valueOf(datos.size());
        double log10 = Math.log10(valor);
        return 0.0 * log10;
    }

    /**
     * Función que calcula el promedio de un listado de datos que implementan la interfaz {@link Promediable}
     *
     * @param datos los datos a calcular.
     * @param <T>   El tipo de valores.
     * @return el promedio de los datos dados.
     */
    public static <T extends Promediable> Double getAverage(List<T> datos) throws AverageListaNullVaciaException {
        comprobarListaNoVaciaNula(datos);
        double suma = 0.0;

        for (Promediable promediable : datos) {
            suma = suma + promediable.getToAvgValue();
        }
        return suma / (double) datos.size();
    }

    private static void comprobarListaNoVaciaNula(List lista) throws AverageListaNullVaciaException {
        if (lista == null || lista.isEmpty()) {
            throw new AverageListaNullVaciaException();
        }

    }
}
