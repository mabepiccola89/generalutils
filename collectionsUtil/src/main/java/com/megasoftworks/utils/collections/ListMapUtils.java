/**
 * Clase ListMapUtils.java mar 19, 2019
 */
package com.megasoftworks.utils.collections;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.megasoftworks.utils.collections.exceptions.ListConversionException;

/**
 * @author pabloaleman
 */
public final class ListMapUtils {

    private static final String CADENA_VACIA = "";
    private static final String ESPACIO = " ";
    private static final String SALTO_LINEA = "\n";
    private static final String MENSAJE_ERROR_CONVERTIR = "Error al convertir";

    private static final Logger LOGGER = LoggerFactory.getLogger(ListMapUtils.class);

    private ListMapUtils() {
    }

    /**
     * Funcion que recibe una cadena de valores enteror separados por un caracter especial y retorna una lista
     * de enteros.
     *
     * @param cadena    la cadena a convertir.
     * @param separador el separador de los enteros en la cadena.
     * @return la lista de enteros.
     * @throws ListConversionException en caso que la cadena no contenta un entero.
     */
    public static List<Integer> stringToIntegerList(String cadena, String separador) throws ListConversionException {
        try {
            cadena = eliminarEspacios(cadena);
            List<Integer> lista = new ArrayList<>();
            String[] arregloCadena = cadena.split(separador);
            for (String valor : arregloCadena) {
                lista.add(Integer.parseInt(valor));
            }
            return lista;
        } catch (NumberFormatException e) {
            LOGGER.error(MENSAJE_ERROR_CONVERTIR, e);
            throw new ListConversionException("Cadena no tiene solo enteros", e);
        } catch (NullPointerException e) {
            LOGGER.error("Error al convertir cadena null", e);
            throw new ListConversionException("Cadena es nulo", e);
        }
    }

    /**
     * Funcion que recibe un arreglo de string que representan numeros enteros.
     * @param lista el listado de enteros en formato string.
     * @return la lista de enteros.
     * @throws ListConversionException en caso de errores.
     */
    public static List<Integer> stringArrayToIntegerList(String[] lista) throws ListConversionException {
        try {
            List<Integer> retorno = new ArrayList<>();
            for (String n : lista) {
                retorno.add(Integer.parseInt(eliminarEspacios(n)));
            }
            return retorno;
        } catch (NumberFormatException e) {
            LOGGER.error(MENSAJE_ERROR_CONVERTIR, e);
            throw new ListConversionException("Cadena no tiene solo enteros", e);
        }
    }

    /**
     * Funcion que recibe un arreglo de string que representan numeros dobles.
     * @param lista el listado de enteros en formato string.
     * @return la lista de enteros.
     * @throws ListConversionException en caso de errores.
     */
    public static List<Double> stringArrayToDoubleList(String[] lista) throws ListConversionException {
        try {
            List<Double> retorno = new ArrayList<>();
            for (String n : lista) {
                retorno.add(Double.parseDouble(eliminarEspacios(n)));
            }
            return retorno;
        } catch (NumberFormatException e) {
            LOGGER.error(MENSAJE_ERROR_CONVERTIR, e);
            throw new ListConversionException("Cadena no tiene solo dobles", e);
        }
    }

    /**
     * Funcion que elimina los espacios y saltos de lineas.
     * @param cadena la cadena a eliminar.
     * @return la cadena transformada.
     */
    private static String eliminarEspacios(String cadena) {
        return cadena.trim().replace(ESPACIO, CADENA_VACIA).replace(SALTO_LINEA, CADENA_VACIA);
    }
}
