/**
 * Clase MailUtil.java mar 19, 2019
 */
package com.megasoftworks.utils.mail;

import com.megasoftworks.utils.mail.exceptions.SendMailException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Properties;

/**
 * @author pabloaleman
 */
public class MailUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(MailUtil.class);
    private static final String MENSAJE_ERROR = "Error al enviar el correo";

    public void enviaCorreo(String fromA,
                            String nombreDespliega, String fromU, String pass, String host,
                            String to, String subject, String text,
                            List<byte[]> archivosAdjuntos, List<String> nombresArchivosAdjuntos,
                            List<String> mimeType,
                            boolean starttls) throws SendMailException {

        try {
            Properties props = armaProperties(starttls, fromU, host);
            Session session = Session.getDefaultInstance(props, null);
            MimeMessage message = armaCabeceraCorreo(session, fromA, nombreDespliega, to, subject);
            Multipart mp = crearCuerpoCorreo(text);

            if (archivosAdjuntos != null && !archivosAdjuntos.isEmpty()) {
                for (int n = 0; n < archivosAdjuntos.size(); n++) {
                    String nombreArchivo = nombresArchivosAdjuntos.get(n);
                    MimeBodyPart mbp2 = new MimeBodyPart();
                    ByteArrayDataSource byteArrayDataSource = new ByteArrayDataSource(archivosAdjuntos.get(n), mimeType.get(n));
                    mbp2.setDataHandler(new DataHandler(byteArrayDataSource));

                    mbp2.setFileName(nombreArchivo);
                    mp.addBodyPart(mbp2);
                }
            }
            message.setContent(mp);
            Transport t = session.getTransport("smtp");
            t.connect(fromU, pass);
            t.sendMessage(message, message.getAllRecipients());
        } catch (Exception e) {
            LOGGER.error(MENSAJE_ERROR, e);
            throw new SendMailException(MENSAJE_ERROR);
        }
    }

    public void enviaCorreo(String fromA,
                            String nombreDespliega, String fromU, String pass, String host,
                            String to, String subject, String text,
                            File archivoAdjunto, String nombreArchivo,
                            boolean starttls) throws SendMailException {

        try {
            Properties props = armaProperties(starttls, fromU, host);
            Session session = Session.getDefaultInstance(props, null);
            MimeMessage message = armaCabeceraCorreo(session, fromA, nombreDespliega, to, subject);
            Multipart mp = crearCuerpoCorreo(text);

            if (archivoAdjunto != null) {
                MimeBodyPart mbp2 = new MimeBodyPart();
                mbp2.setDataHandler(new DataHandler(
                        new FileDataSource(archivoAdjunto)));
                mbp2.setFileName(nombreArchivo);
                mp.addBodyPart(mbp2);
            }
            message.setContent(mp);
            Transport t = session.getTransport("smtp");
            t.connect(fromU, pass);
            t.sendMessage(message, message.getAllRecipients());
        } catch (Exception e) {
            LOGGER.error(MENSAJE_ERROR, e);
            throw new SendMailException(MENSAJE_ERROR);
        }
    }

    private MimeMessage armaCabeceraCorreo(Session session, String fromA, String nombreDespliega, String to, String subject) throws SendMailException {
        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(fromA, nombreDespliega));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject(subject);
            return message;
        } catch (UnsupportedEncodingException | MessagingException e) {
            throw new SendMailException("Error creando la cabecera del correo", e);
        }
    }

    private Multipart crearCuerpoCorreo(String texto) throws SendMailException {
        try {
            Multipart mp = new MimeMultipart();
            MimeBodyPart mbp = new MimeBodyPart();
            mbp.setText(texto, "ISO-8859-1", "html");
            mp.addBodyPart(mbp);
            return mp;
        } catch (MessagingException e) {
            String mensaje = "Error creando el cuerpo del mensaje";
            LOGGER.error(mensaje, e);
            throw new SendMailException(mensaje);
        }
    }

    private Properties armaProperties(boolean starttls, String fromU, String host) {
        Properties props = new Properties();
        if (starttls) {
            props.setProperty("mail.smtp.starttls.enable", "true");
        }

        props.setProperty("mail.smtp.port", "25");
        props.setProperty("mail.smtp.user", fromU);
        props.setProperty("mail.smtp.auth", "true");
        props.put("mail.smtp.host", host);
        return props;
    }


}