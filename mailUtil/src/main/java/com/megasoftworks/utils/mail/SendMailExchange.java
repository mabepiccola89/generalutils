/**
 * Clase SendMailExchange.java mar 19, 2019
 */
package com.megasoftworks.utils.mail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * @author pabloaleman
 */
public final class SendMailExchange {
    private static final Logger LOGGER = LoggerFactory.getLogger(SendMailExchange.class);


    /**
     * Constructor por default.
     */
    private SendMailExchange() {
    }

    /**
     * Funcion que envia un correo.
     *
     * @param host     el servidor.
     * @param portSMTP el puerto smtp.
     * @param fromA    la direccion del remitente.
     * @param fromU    el nombre de usuario del remitente.
     * @param pass     el password.
     * @param to       a quien.
     * @param subject  el asunto.
     * @param text     el texto.
     * @param starttls si existe seguridad starttls.
     * @return si se envio o no el correo.
     */
    public static boolean sendMail(String host, String portSMTP,
                                   String fromA, String fromU, String pass,
                                   List<String> to, String subject, String text,
                                   String starttls) {
        boolean retorno;
        try {
            Properties props = new Properties();
            if ("true".equals(starttls)) {
                props.setProperty("mail.smtp.starttls.enable", "true");
            }

            props.setProperty("mail.smtp.port", portSMTP);
            props.setProperty("mail.smtp.user", fromU);
            props.setProperty("mail.smtp.auth", "true");
            props.put("mail.smtp.host", host);
            Session session = Session.getDefaultInstance(props, null);
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(fromA));
            for (String tosS : to) {
                message.addRecipient(Message.RecipientType.BCC,
                        new InternetAddress(tosS));
            }
            message.setSubject(subject);

            Multipart mp = new MimeMultipart();
            MimeBodyPart mbp = new MimeBodyPart();
            mbp.setText(text, "ISO-8859-1", "html");
            mp.addBodyPart(mbp);

            message.setContent(mp);
            Transport t = session.getTransport("smtp");
            t.connect(fromU, pass);
            t.sendMessage(message, message.getAllRecipients());
            LOGGER.debug("Message sent to: {}", to);
            retorno = true;
        } catch (Exception exp) {
            LOGGER.error("Exception are :: ", exp);
            retorno = false;
        }
        return retorno;
    }

}
