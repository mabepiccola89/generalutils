/**
 * Clase LeerCorreo.java mar 19, 2019
 */
package com.megasoftworks.utils.mail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Store;

/**
 * @author pabloaleman
 */
public class LeerCorreo {

    private static final Logger LOGGER = LoggerFactory.getLogger(LeerCorreo.class);

    /**
     * Funcion que lee correos electronicos.
     * @param host el servidor de correo
     * @param username el nombre de usuario
     * @param password las contrasena
     * @param folder la carpeta a leer
     */
    public List<Message> leeCorreo(String host, String username, String password,
            String folder) {
        List<Message> retorno = new ArrayList<Message>();
        try {
            Properties props = new Properties();
            Session sess = Session.getDefaultInstance(props, null);
            // Get the store
            Store store = sess.getStore("pop3");
            store.connect(host, username, password);
            // Get folder
            Folder fold = store.getFolder(folder);
            fold.open(Folder.READ_ONLY);
            // Get directory
            Message[] messages = fold.getMessages();
            
            retorno.addAll(Arrays.asList(messages));
            fold.close(false);
            store.close();
        } catch (Exception e) {
            LOGGER.error("Error al leer el correo", e);
        }
        return retorno;

    }

}
