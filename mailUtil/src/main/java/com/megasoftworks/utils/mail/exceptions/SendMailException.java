/**
 * Clase SendMailException.java mar 19, 2019
 */
package com.megasoftworks.utils.mail.exceptions;

/**
 * @author pabloaleman
 */
public class SendMailException extends Exception {

    private static final long serialVersionUID = -4672587120662113109L;

    public SendMailException(String message, Throwable cause) {
        super(message, cause);
    }

    public SendMailException(String mess) {
        super(mess);
    }
}
